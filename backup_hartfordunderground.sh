#!/bin/bash

# Backup DB
ssh hart8594@hartfordunderground.com bash db_backup.sh

# Pull any changes first
cd $HOME/workspace/hartford_underground_live
legit sync

# Backup whole site
username=hart8594
host=hartfordunderground.com
remote_location=/var/chroot/home/content/31/12228231/html/*
destination=$HOME/workspace/hartford_underground_live/
rsync -auvz ${username}@${host}:${remote_location} ${destination}

# Commit and push the automatic backup
git add .
git commit -m "Automatic backup"
legit sync
