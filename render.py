import os
import jinja2
import maya
import calendar


c = calendar.Calendar(firstweekday=calendar.SUNDAY)


def render(template_path, context):
    path, filename = os.path.split(template_path)
    return (
        jinja2.Environment(loader=jinja2.FileSystemLoader(path or "./"))
        .get_template(filename)
        .render(context)
    )


def render_and_write(template_path, context):
    filename = "Output/{}.html".format("_".join(context["month_string"].split()))
    with open(filename, "w") as html_file:
        html_file.write(render("layout.html", context))
