import os
import sys
import requests
import json
import platform
import maya
from maya import MayaDT
from pytz import timezone
from bs4 import BeautifulSoup
from collections import namedtuple
from selenium import webdriver
from logger import logger
from render import render, render_and_write


Event = namedtuple("Event", ["name", "url"])
DateOption = namedtuple("DateOption", ["eid", "date"])


def init_driver():
    if "microsoft" in platform.platform().lower():
        # You're running in Windows Subsystem for Linux,
        # so use xvfb and pyvirtualdisplay to simulate headless mode
        import pyvirtualdisplay

        display = pyvirtualdisplay.Display(visible=0, size=(1920, 1080))
        display.start()
    else:
        # Otherwise just run headless mode
        os.environ["MOZ_HEADLESS"] = "1"

    return webdriver.Firefox()


def get_date(date_str):
    days_of_week = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    # Add one day to each event date since they're used to determine how long
    # an event is considered active
    try:
        dt = (
            maya.when(date_str)
            .add(days=1)
            .datetime()
            .astimezone(timezone("US/Eastern"))
            .strftime("%m-%d-%Y")
        )

        for day in days_of_week:
            if day in date_str:
                return dt
    except Exception as e:
        return None


def get_live_events(driver, main_page=None):
    if not main_page:
        main_page = "https://www.eventbrite.com/o/hartford-underground-5705650923"

    driver.get(main_page)
    soup = BeautifulSoup(driver.page_source, "lxml")

    events = []

    for e in soup.select("#live_events > div > a"):
        url = e["href"]
        title = (
            e.find("div", class_="list-card__title")
            .text.replace("\n", "")
            .replace("Hartford Underground: ", "")
            .strip()
        )

        events.append(Event(title, url))

    return events


def get_event_info(event, driver):
    driver.get(event.url)
    soup = BeautifulSoup(driver.page_source, "lxml")

    body = soup.find("div", class_="js-xd-read-more-contents")

    description = str(body.select("p")[1])
    if description.startswith("Social Dances:") or len(description) < 80:
        # Descriptions can't be that short; it must be blank
        description = ""

    # Get EIDs and their dates from the options menu if they contain a date
    options = soup.find("select", class_="js-d-select-box").findAll("option")
    date_options = [
        date_option
        for date_option in [DateOption(x["value"], get_date(x.text)) for x in options]
        if date_option.date
    ]

    month_str = maya.when(date_options[0].date).datetime().strftime("%B %Y")
    eids_str = str([x.eid for x in date_options])[1:][:-1]

    info = {
        "month_string": month_str,
        "type": event.name,
        "title": body.find("h2").text,
        "description": description,
        "week_1": date_options[0].date,
        "week_2": date_options[1].date,
        "week_3": date_options[2].date,
        "week_4": date_options[3].date,
        "list_of_eids": eids_str,
        "num_eids": len(date_options),
    }

    return info


def get_context_from_user():
    month_name = input("Month name: ")
    year = int(input("Year: "))
    event_type = input("Event Type: ")
    title = input("Title: ")
    list_of_eids = list(
        input("List of eids: ")
        .replace("[", "")
        .replace("]", "")
        .replace("'", "")
        .replace('"', "")
        .replace(" ", "")
        .split(",")
    )
    repeats = input("What day of the week does the event repeat on? ")
    first_day = int(input("Which {} to start on? (1 or 2) ".format(repeats)))
    if first_day < 1 or first_day > 2:
        exit(
            "You can only start on the first or second week since there are "
            "at least 4 weeks in a class."
        )

    description = input("Paste the description here: ")

    month_string = "{} {}".format(month_name, year)
    month = maya.when(month_string).month
    month_cal = c.monthdatescalendar(year, month)
    weekday_to_repeat = maya.when(repeats).weekday - 1

    days_after_events = [
        day.strftime("%m-%d-%Y")
        for week in month_cal
        for day in week
        if day.weekday() == weekday_to_repeat and day.month == month
    ]

    context = {
        "month_string": month_string,
        "type": event_type,
        "title": title,
        "description": description,
        "week_1": days_after_events[first_day],
        "week_2": days_after_events[first_day + 1],
        "week_3": days_after_events[first_day + 2],
        "week_4": days_after_events[first_day + 3],
        "list_of_eids": str(list_of_eids).replace("[", "").replace("]", ""),
        "num_eids": len(list_of_eids),
    }

    return context


def render_and_save_event(event):
    try:
        context = get_event_info(event, driver)
        render_and_write("layout.html", context)
    except AttributeError:
        filename = "Output/{}.html".format("_".join(event.name.split()))
        logger.debug(
            "Couldn't find the date options for "
            "{}.  Skipping it...".format(event.name)
        )

        with open(filename, "w") as html_file:
            html_file.write(driver.page_source)

    except IndexError:
        logger.debug(
            "Less than 4 events found for {}.  " "Skipping it...".format(event.name)
        )
        return False

    return True


if __name__ == "__main__":
    driver = init_driver()
    events = get_live_events(driver)

    for event in events:
        render_and_save_event(event)
